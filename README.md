# go_lib

> go lib

[![codecov](https://codecov.io/gl/qiankaihua/go_lib/branch/main/graph/badge.svg?token=09PVSHN5WG)](https://codecov.io/gl/qiankaihua/go_lib)
[![pipeline status](https://gitlab.com/qiankaihua/go_lib/badges/main/pipeline.svg)](https://gitlab.com/qiankaihua/go_lib/-/commits/main)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/qiankaihua/go_lib)](https://goreportcard.com/report/gitlab.com/qiankaihua/go_lib)

> all data structure is not thread safe

- list
    - link_list
    - circular_link_list
- stack
- tree
    - sort_binary_tree
- set

> thread safe

- cache
    - simple map + mutex