package simple

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestHashCache(t *testing.T) {
	a := require.New(t)
	cache := NewCache[int]()
	a.NotNil(cache)
	cache.Set("123", 123)
	v, ok := cache.Get("123")
	a.True(ok)
	a.Equal(v, 123)
	v, ok = cache.Get("not exist")
	a.False(ok)
	a.Equal(v, 0)
	cache.SetWithTtl("time", 1, time.Millisecond*10)
	v, ok = cache.Get("time")
	a.True(ok)
	a.Equal(v, 1)
	time.Sleep(time.Millisecond * 10)
	v, ok = cache.Get("time")
	a.False(ok)
	a.Equal(v, 0)
}
