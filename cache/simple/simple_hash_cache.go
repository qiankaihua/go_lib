package simple

import (
	"sync"
	"time"

	"gitlab.com/qiankaihua/go_lib/cache"
)

type SimpleCache[T any] cache.Cache[string, T]

type Cache[T any] struct {
	data map[string]*cacheItem[T]
	lock sync.RWMutex
}

type cacheItem[T any] struct {
	data *T
	ddl  *time.Time
}

func NewCache[T any]() SimpleCache[T] {
	return &Cache[T]{
		data: map[string]*cacheItem[T]{},
		lock: sync.RWMutex{},
	}
}

func (c *Cache[T]) Get(key string) (value T, ok bool) {
	c.lock.RLock()
	defer c.lock.RUnlock()
	item, ok := c.data[key]
	if !ok {
		return
	}
	if item == nil || item.data == nil || (item.ddl != nil && item.ddl.Before(time.Now())) {
		ok = false
		delete(c.data, key)
		return
	}
	return *item.data, true
}

func (c *Cache[T]) Set(key string, value T) {
	c.SetWithTtl(key, value, 0)
}

func (c *Cache[T]) SetWithTtl(key string, value T, ttl time.Duration) {
	item := cacheItem[T]{
		data: &value,
		ddl:  nil,
	}
	if ttl > 0 {
		ddl := time.Now().Add(ttl)
		item.ddl = &ddl
	}
	c.lock.Lock()
	defer c.lock.Unlock()
	c.data[key] = &item
}
