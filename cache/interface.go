package cache

import "time"

type Cache[K, V any] interface {
	Get(key K) (V, bool)
	Set(key K, value V)
	SetWithTtl(key K, value V, ttl time.Duration)
}
