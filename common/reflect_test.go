package common

import (
	"testing"

	"github.com/stretchr/testify/require"
)

type test struct{}

func returnInterfaceNil() interface{} {
	var a *test
	return a
}

func TestIsInterfaceNil(t *testing.T) {
	a := require.New(t)
	a.True(IsInterfaceNil(nil))
	a.True(IsInterfaceNil(returnInterfaceNil()))
	a.False(returnInterfaceNil() == nil)
}
