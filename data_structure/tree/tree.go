package tree

type ITreeNode[K, V any] interface {
	Right() ITreeNode[K, V]
	Left() ITreeNode[K, V]
	Parent() ITreeNode[K, V]

	Key() K
	Value() V
	IsNil() bool
	String() string
}

type TreeIterator[K, V any] struct {
	node  ITreeNode[K, V]
	valid bool
}

func NewTreeIterator[K, V any](node ITreeNode[K, V]) *TreeIterator[K, V] {
	return &TreeIterator[K, V]{
		node:  node,
		valid: true,
	}
}

func (iter *TreeIterator[K, V]) Next() {
	if isNodeNil(iter.node) {
		return
	}
	if !isNodeNil(iter.node.Right()) {
		cur := iter.node.Right()
		for !isNodeNil(cur.Left()) {
			cur = cur.Left()
		}
		iter.node = cur
		return
	}
	cur := iter.node.Parent()
	for !isNodeNil(cur) && iter.node == cur.Right() {
		iter.node = cur
		cur = cur.Parent()
	}
	iter.node = cur
}

func (iter *TreeIterator[K, V]) Prev() {
	if isNodeNil(iter.node) {
		return
	}
	if !isNodeNil(iter.node.Left()) {
		cur := iter.node.Left()
		for !isNodeNil(cur.Right()) {
			cur = cur.Right()
		}
		iter.node = cur
		return
	}
	cur := iter.node.Parent()
	for !isNodeNil(cur) && iter.node == cur.Left() {
		iter.node = cur
		cur = cur.Parent()
	}
	iter.node = cur
}

func (iter *TreeIterator[K, V]) IsEnd() bool {
	if !iter.valid || isNodeNil(iter.node) {
		return true
	}
	return false
}

func (iter *TreeIterator[K, V]) SetInvalid() {
	iter.valid = false
}

func (iter *TreeIterator[K, V]) Value() (V, bool) {
	if isNodeNil(iter.node) {
		return *new(V), false
	}
	return iter.node.Value(), true
}

func (iter *TreeIterator[K, V]) Key() (K, bool) {
	if isNodeNil(iter.node) {
		return *new(K), false
	}
	return iter.node.Key(), true
}

func PrintITreeNode[K, V any](n ITreeNode[K, V], prefix string, isHead bool, outString *string) {
	if n == nil || n.IsNil() {
		return
	}
	if !isNodeNil(n.Right()) {
		newPrefix := prefix
		if isHead {
			newPrefix += "|   "
		} else {
			newPrefix += "    "
		}
		PrintITreeNode(n.Right(), newPrefix, false, outString)
	}
	*outString += prefix
	if isHead {
		*outString += "└── "
	} else {
		*outString += "┌── "
	}
	*outString += n.String() + "\n"
	if !isNodeNil(n.Left()) {
		newPrefix := prefix
		if isHead {
			newPrefix += "    "
		} else {
			newPrefix += "|   "
		}
		PrintITreeNode(n.Left(), newPrefix, true, outString)
	}
}

func isNodeNil[K, V any](node ITreeNode[K, V]) bool {
	return node == nil || node.IsNil()
}
