package sort_binary_tree

import (
	"fmt"

	"gitlab.com/qiankaihua/go_lib/common"
	"gitlab.com/qiankaihua/go_lib/data_structure/tree"
)

type SortBinaryTree[K, V any] struct {
	root        *binaryTreeNode[K, V]
	compareFunc func(left, right K) int
}

func NewSortBinaryTree[K common.Ordered, V any]() *SortBinaryTree[K, V] {
	return NewSortBinaryTreeWithCompareFunc[K, V](common.CompareOrdered[K])
}

func NewSortBinaryTreeWithCompareFunc[K, V any](compareFunc func(left, right K) int) *SortBinaryTree[K, V] {
	return &SortBinaryTree[K, V]{
		compareFunc: compareFunc,
	}
}

type SortBinaryTreeIterator[K, V any] struct {
}

type binaryTreeNode[K, V any] struct {
	key    K
	value  V
	left   *binaryTreeNode[K, V]
	right  *binaryTreeNode[K, V]
	parent *binaryTreeNode[K, V]
}

func (bt *SortBinaryTree[K, V]) Insert(key K, value V) {
	if bt.root == nil {
		bt.root = &binaryTreeNode[K, V]{
			key:   key,
			value: value,
		}
		return
	}
	cur := bt.root
	for cur != nil {
		cmp := bt.compareFunc(cur.key, key)
		switch {
		case cmp > 0:
			if cur.left == nil {
				cur.left = &binaryTreeNode[K, V]{
					key:    key,
					value:  value,
					parent: cur,
				}
				return
			}
			cur = cur.left
		case cmp < 0:
			if cur.right == nil {
				cur.right = &binaryTreeNode[K, V]{
					key:    key,
					value:  value,
					parent: cur,
				}
				return
			}
			cur = cur.right
		case cmp == 0:
			cur.value = value
			return
		}
	}
}

func (bt *SortBinaryTree[K, V]) Remove(key K) (V, bool) {
	node := bt.find(key)
	if node == nil {
		return *new(V), false
	}
	if node.left == nil {
		if node.parent != nil {
			if node.parent.left == node {
				node.parent.left = node.right
			} else {
				node.parent.right = node.right
			}
			if node.right != nil {
				node.right.parent = node.parent
			}
		} else {
			bt.root = node.right
			if bt.root != nil {
				bt.root.parent = nil
			}
		}
	} else {
		lmax := node.left.max()
		lmax.right = node.right
		if node.right != nil {
			node.right.parent = lmax
		}
		if lmax.parent.left == lmax {
			lmax.parent.left = nil
		} else {
			lmax.parent.right = nil
		}
		if node.parent == nil {
			lmax.parent = nil
			bt.root = lmax
			if node.left != lmax && node.left != nil {
				lmax.left = node.left
				node.left.parent = lmax
			}
		} else {
			lmax.parent = node.parent
			if node.left != lmax && node.left != nil {
				node.left.parent = lmax.min()
				lmax.min().left = node.left
			}
			if node.parent.left == node {
				node.parent.left = lmax
			} else {
				node.parent.right = lmax
			}
		}
	}
	return node.value, true
}

func (bt *SortBinaryTree[K, V]) Search(key K) (V, bool) {
	node := bt.find(key)
	if node == nil {
		return *new(V), false
	}
	return node.value, true
}

func (bt *SortBinaryTree[K, V]) Begin() *tree.TreeIterator[K, V] {
	if bt.root == nil {
		return tree.NewTreeIterator[K, V](bt.root)
	}
	return tree.NewTreeIterator[K, V](bt.root.min())
}

func (bt *SortBinaryTree[K, V]) End() *tree.TreeIterator[K, V] {
	if bt.root == nil {
		return tree.NewTreeIterator[K, V](bt.root)
	}
	return tree.NewTreeIterator[K, V](bt.root.max())
}

func (bt *SortBinaryTree[K, V]) Ceil(key K, equal bool) *tree.TreeIterator[K, V] {
	if bt.root == nil {
		return tree.NewTreeIterator[K, V](bt.root)
	}
	cur := bt.root
	found := false
	last := bt.root
	for cur != nil {
		cmp := bt.compareFunc(cur.key, key)
		switch {
		case cmp == 0:
			iter := tree.NewTreeIterator[K, V](cur)
			if !equal {
				iter.Next()
			}
			return iter
		case cmp > 0:
			last = cur
			found = true
			cur = cur.left
		case cmp < 0:
			cur = cur.right
		}
	}
	if found {
		return tree.NewTreeIterator[K, V](last)
	}
	return tree.NewTreeIterator[K, V](nil)
}

func (bt *SortBinaryTree[K, V]) Floor(key K, equal bool) *tree.TreeIterator[K, V] {
	if bt.root == nil {
		return tree.NewTreeIterator[K, V](bt.root)
	}
	cur := bt.root
	found := false
	last := bt.root
	for cur != nil {
		cmp := bt.compareFunc(cur.key, key)
		switch {
		case cmp == 0:
			iter := tree.NewTreeIterator[K, V](cur)
			if !equal {
				iter.Prev()
			}
			return iter
		case cmp > 0:
			cur = cur.left
		case cmp < 0:
			last = cur
			found = true
			cur = cur.right
		}
	}
	if found {
		return tree.NewTreeIterator[K, V](last)
	}
	return tree.NewTreeIterator[K, V](nil)
}

func (bt *SortBinaryTree[K, V]) String() string {
	outString := "BinaryTree:\n"
	tree.PrintITreeNode[K, V](bt.root, "", true, &outString)
	return outString
}

// ================= helper function =================

func (bt *SortBinaryTree[K, V]) find(key K) *binaryTreeNode[K, V] {
	now := bt.root
	for now != nil {
		cmp := bt.compareFunc(now.key, key)
		switch {
		case cmp == 0:
			return now
		case cmp > 0:
			now = now.left
		case cmp < 0:
			now = now.right
		}
	}
	return nil
}

func (n *binaryTreeNode[K, V]) String() string {
	return fmt.Sprintf("(%v:%v)", n.key, n.value)
}
func (n *binaryTreeNode[K, V]) Left() tree.ITreeNode[K, V] {
	return n.left
}
func (n *binaryTreeNode[K, V]) Right() tree.ITreeNode[K, V] {
	return n.right
}
func (n *binaryTreeNode[K, V]) Parent() tree.ITreeNode[K, V] {
	return n.parent
}
func (n *binaryTreeNode[K, V]) Value() V {
	return n.value
}
func (n *binaryTreeNode[K, V]) Key() K {
	return n.key
}
func (n *binaryTreeNode[K, V]) IsNil() bool {
	return n == nil
}

func (n *binaryTreeNode[K, V]) max() *binaryTreeNode[K, V] {
	if n.right == nil {
		return n
	}
	return n.right.max()
}

func (n *binaryTreeNode[K, V]) min() *binaryTreeNode[K, V] {
	if n.left == nil {
		return n
	}
	return n.left.min()
}
