package sort_binary_tree

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestSortBinaryTree(t *testing.T) {
	a := require.New(t)
	// new
	bt := NewSortBinaryTree[int, int]()
	{
		a.NotNil(bt)
		a.Equal("BinaryTree:\n", bt.String())
		a.NotNil(bt.Begin())
		a.NotNil(bt.End())
		a.NotNil(bt.Floor(1, true))
		a.NotNil(bt.Ceil(1, true))
	}

	treeMap := map[int]struct{}{}

	// insert
	{
		// insert empty tree
		bt.Insert(1, 1)
		treeMap[1] = struct{}{}
		a.NotNil(bt.root)
		a.Equal(int(1), bt.root.key)
		a.Equal(1, bt.root.value)
		// insert right
		bt.Insert(10, 10)
		treeMap[10] = struct{}{}
		a.NotNil(bt.root.right)
		a.Equal(10, bt.root.right.value)
		// insert right-right
		bt.Insert(15, 15)
		treeMap[15] = struct{}{}
		a.NotNil(bt.root.right.right)
		a.Equal(15, bt.root.right.right.value)
		// insert right-left
		bt.Insert(5, 5)
		treeMap[5] = struct{}{}
		a.NotNil(bt.root.right.left)
		a.Equal(5, bt.root.right.left.value)
		// insert left
		bt.Insert(-10, -10)
		treeMap[-10] = struct{}{}
		a.NotNil(bt.root.left)
		a.Equal(-10, bt.root.left.value)
		// insert left-left
		bt.Insert(-15, -15)
		treeMap[-15] = struct{}{}
		a.NotNil(bt.root.left.left)
		a.Equal(-15, bt.root.left.left.value)
		// insert left-right
		bt.Insert(-5, -5)
		treeMap[-5] = struct{}{}
		a.NotNil(bt.root.left.right)
		a.Equal(-5, bt.root.left.right.value)
		// insert exist
		bt.Insert(1, 0)
		a.NotNil(bt.root)
		a.Equal(0, bt.root.value)
	}

	// iterator
	{
		// begin
		iter := bt.Begin()
		cnt := 0
		for !iter.IsEnd() {
			k, ok := iter.Key()
			a.True(ok)
			a.Contains(treeMap, k)
			_, ok = iter.Value()
			a.True(ok)
			iter.Next()
			cnt++
		}
		iter.Next()
		k, ok := iter.Key()
		a.False(ok)
		a.Zero(k)
		v, ok := iter.Value()
		a.False(ok)
		a.Zero(v)
		a.Equal(len(treeMap), cnt)

		// end
		iter = bt.End()
		cnt = 0
		for !iter.IsEnd() {
			k, ok := iter.Key()
			a.True(ok)
			a.Contains(treeMap, k)
			_, ok = iter.Value()
			a.True(ok)
			iter.Prev()
			cnt++
		}
		iter.Prev()
		k, ok = iter.Key()
		a.False(ok)
		a.Zero(k)
		v, ok = iter.Value()
		a.False(ok)
		a.Zero(v)
		a.Equal(len(treeMap), cnt)

		// invalid
		iter = bt.Begin()
		iter.SetInvalid()
		a.True(iter.IsEnd())
	}

	// toString
	{
		treeString := `BinaryTree:
|       ┌── (15:15)
|   ┌── (10:10)
|   |   └── (5:5)
└── (1:0)
    |   ┌── (-5:-5)
    └── (-10:-10)
        └── (-15:-15)
`
		a.Equal(treeString, bt.String())
	}

	// search
	{
		// root
		v, ok := bt.Search(1)
		a.True(ok)
		a.Equal(0, v)
		// right
		v, ok = bt.Search(10)
		a.True(ok)
		a.Equal(10, v)
		// right - left
		v, ok = bt.Search(5)
		a.True(ok)
		a.Equal(5, v)
		// right - right
		v, ok = bt.Search(15)
		a.True(ok)
		a.Equal(15, v)
		// left
		v, ok = bt.Search(-10)
		a.True(ok)
		a.Equal(-10, v)
		// left - left
		v, ok = bt.Search(-15)
		a.True(ok)
		a.Equal(-15, v)
		// left - right
		v, ok = bt.Search(-5)
		a.True(ok)
		a.Equal(-5, v)
		// not found
		v, ok = bt.Search(999)
		a.False(ok)
		a.Equal(0, v)
	}

	// ceil / floor
	{
		// ceil find not equal
		iter := bt.Ceil(1, false)
		targetKey := int(9999999)
		for k := range treeMap {
			if k > 1 && k < targetKey {
				targetKey = k
			}
		}
		key, ok := iter.Key()
		a.True(ok)
		a.Equal(key, targetKey)
		// ceil find equal
		iter = bt.Ceil(1, true)
		targetKey = int(9999999)
		for k := range treeMap {
			if k >= 1 && k < targetKey {
				targetKey = k
			}
		}
		key, ok = iter.Key()
		a.True(ok)
		a.Equal(key, targetKey)
		// ceil not find equal
		iter = bt.Ceil(2, true)
		targetKey = int(9999999)
		for k := range treeMap {
			if k >= 2 && k < targetKey {
				targetKey = k
			}
		}
		key, ok = iter.Key()
		a.True(ok)
		a.Equal(key, targetKey)
		// ceil max
		iter = bt.Ceil(9999999, true)
		_, ok = iter.Key()
		a.False(ok)

		// floor find not equal
		iter = bt.Floor(1, false)
		targetKey = int(-9999999)
		for k := range treeMap {
			if k < 1 && k > targetKey {
				targetKey = k
			}
		}
		key, ok = iter.Key()
		a.True(ok)
		a.Equal(key, targetKey)
		// floor find equal
		iter = bt.Floor(1, true)
		targetKey = int(-9999999)
		for k := range treeMap {
			if k <= 1 && k > targetKey {
				targetKey = k
			}
		}
		key, ok = iter.Key()
		a.True(ok)
		a.Equal(key, targetKey)
		// floor not find equal
		iter = bt.Floor(2, true)
		targetKey = int(-9999999)
		for k := range treeMap {
			if k <= 2 && k > targetKey {
				targetKey = k
			}
		}
		key, ok = iter.Key()
		a.True(ok)
		a.Equal(key, targetKey)
		// floor min
		iter = bt.Floor(-9999999, true)
		_, ok = iter.Key()
		a.False(ok)
	}

	// delete
	{
		// root
		bt.Remove(1)
		delete(treeMap, 1)
		testTree("1", a, bt, treeMap)
		// left is nil
		bt.Remove(5)
		delete(treeMap, 5)
		testTree("2", a, bt, treeMap)
		bt.Remove(10)
		delete(treeMap, 10)
		testTree("3", a, bt, treeMap)
		// left is not nil
		bt.Remove(-10)
		delete(treeMap, -10)
		testTree("4", a, bt, treeMap)

		bt.Insert(-16, -16)
		treeMap[-16] = struct{}{}
		bt.Insert(-19, -19)
		treeMap[-19] = struct{}{}
		bt.Insert(-17, -17)
		treeMap[-17] = struct{}{}
		bt.Insert(-18, -18)
		treeMap[-18] = struct{}{}
		bt.Remove(-17)
		delete(treeMap, -17)
		testTree("5", a, bt, treeMap)
		bt.Remove(-16)
		delete(treeMap, -16)
		testTree("6", a, bt, treeMap)
		bt.Remove(-15)
		delete(treeMap, -15)
		bt.Remove(-18)
		delete(treeMap, -18)
		bt.Remove(-19)
		delete(treeMap, -19)
		bt.Remove(-5)
		delete(treeMap, -5)
		testTree("7", a, bt, treeMap)

		// not exist
		bt.Remove(-11111)
		testTree("-1", a, bt, treeMap)
	}
}

func testTree[K any, V any](cases string, a *require.Assertions, bt *SortBinaryTree[K, V], treeMap map[int]struct{}) {
	iter := bt.Begin()
	cnt := 0
	first := true
	pre := *new(K)
	for !iter.IsEnd() {
		k, ok := iter.Key()
		a.True(ok)
		a.Contains(treeMap, k)
		if !first {
			a.Less(bt.compareFunc(pre, k), 0)
		}
		_, ok = iter.Value()
		a.True(ok)
		iter.Next()
		cnt++
		pre = k
		first = false
	}
	a.Equal(len(treeMap), cnt, cases)

}
