package link_list

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestCircularLinkList(t *testing.T) {
	a := require.New(t)
	list := NewCircularLinkList[int]()
	a.NotNil(list)
	a.True(checkCircularEqual(list, 0))
	list.PushAfter(1)
	a.True(checkCircularEqual(list, 0, 1))
	list.PushAfter(2)
	a.True(checkCircularEqual(list, 0, 1, 2))
	list.PushAfter(3)
	a.True(checkCircularEqual(list, 0, 1, 3, 2))
	list.PushBefore(-1)
	a.True(checkCircularEqual(list, 1, -1, 1, 3, 2))
	list.PushBefore(-2)
	a.True(checkCircularEqual(list, 2, -1, -2, 1, 3, 2))
	v, ok := list.RemoveAfter()
	a.Equal(3, v)
	a.True(ok)
	a.True(checkCircularEqual(list, 2, -1, -2, 1, 2))
	v, ok = list.RemoveAfter()
	a.Equal(2, v)
	a.True(ok)
	a.True(checkCircularEqual(list, 2, -1, -2, 1))
	v, ok = list.RemoveAfter()
	a.Equal(-1, v)
	a.True(ok)
	a.True(checkCircularEqual(list, 1, -2, 1))
	v, ok = list.RemoveAfter()
	a.Equal(-2, v)
	a.True(ok)
	a.True(checkCircularEqual(list, 0, 1))
	v, ok = list.RemoveAfter()
	a.Equal(1, v)
	a.True(ok)
	a.True(checkCircularEqual(list, 0))
	v, ok = list.RemoveAfter()
	a.Equal(0, v)
	a.False(ok)
	a.True(checkCircularEqual(list, 0))
	list.PushBefore(1).PushBefore(2).PushBefore(3).PushBefore(4)
	a.True(checkCircularEqual(list, 0, 1, 2, 3, 4))
	a.Equal(int32(4), list.Size())
	a.False(list.Empty())
	v, ok = list.Get()
	a.Equal(1, v)
	a.True(ok)
	list.MoveNext()
	v, ok = list.Get()
	a.Equal(2, v)
	a.True(ok)
	list.MovePrev().MovePrev()
	v, ok = list.Get()
	a.Equal(4, v)
	a.True(ok)
	list.MoveNext()
	v, ok = list.RemoveBefore()
	a.Equal(4, v)
	a.True(ok)
	v, ok = list.RemoveBefore()
	a.Equal(3, v)
	a.True(ok)
	v, ok = list.RemoveBefore()
	a.Equal(2, v)
	a.True(ok)
	v, ok = list.RemoveBefore()
	a.Equal(1, v)
	a.True(ok)
	v, ok = list.RemoveBefore()
	a.Equal(0, v)
	a.False(ok)
	list.PushAfter(1).PushAfter(2).PushAfter(3).PushAfter(4).PushAfter(5)
	a.Equal(int32(5), list.Size())
	v, ok = list.Get()
	a.Equal(1, v)
	a.True(ok)
	cnt := 0
	list.Traversal(func(value int) (needStop bool) {
		cnt++
		return cnt > 2
	}, true)
	v, ok = list.Get()
	a.Equal(1, v)
	a.True(ok)
	list.Clear()
	a.Equal(int32(0), list.Size())
	v, ok = list.Get()
	a.Equal(0, v)
	a.False(ok)
}

func checkCircularEqual(list *CircularLinkList[int], startAt int, shouldBe ...int) (bool, string) {
	pos := 0
	var (
		equal   = true
		message = ""
	)
	if len(shouldBe) != int(list.cnt) {
		return false, fmt.Sprintf("need len %d, got %d", len(shouldBe), list.cnt)
	}
	list.Traversal(func(value int) (needStop bool) {
		if value != shouldBe[(pos+startAt)%int(list.cnt)] {
			equal = false
			message = fmt.Sprintf("at pos %d, need %d, got %d", (pos+startAt)%int(list.cnt), shouldBe[pos], value)
			return true
		}
		pos++
		return false
	}, true)
	return equal, message
}
