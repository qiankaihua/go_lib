package link_list

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestLinkList(t *testing.T) {
	a := require.New(t)
	list := NewLinkList[int]()
	a.NotNil(list)
	a.True(checkEqual(list))
	list.PushTail(1).PushTail(2).PushTail(3)
	a.True(checkEqual(list, 1, 2, 3))
	a.Equal("[head]->(1)->(2)->(3)->[tail]", list.String())
	a.Equal(int32(3), list.Size())
	list.PushHead(-1).PushHead(-2).PushHead(-3)
	a.True(checkEqual(list, -3, -2, -1, 1, 2, 3))
	a.Equal(int32(6), list.Size())
	v, ok := list.GetNow()
	a.False(ok)
	a.Equal(0, v)
	list.InsertBeforeNow(0)
	a.True(checkEqual(list, 0, -3, -2, -1, 1, 2, 3))
	a.Equal(int32(7), list.Size())
	// now at -1
	list.MoveNext().MoveNext().MoveNext().MoveNext()
	v, ok = list.GetNow()
	a.True(ok)
	a.Equal(-1, v)

	list.InsertAfterNow(0).InsertBeforeNow(0)
	a.True(checkEqual(list, 0, -3, -2, 0, -1, 0, 1, 2, 3))
	a.Equal(int32(9), list.Size())

	v, ok = list.RemoveHead()
	a.True(ok)
	a.Equal(0, v)
	a.True(checkEqual(list, -3, -2, 0, -1, 0, 1, 2, 3))
	a.Equal(int32(8), list.Size())
	v, ok = list.RemoveTail()
	a.True(ok)
	a.Equal(3, v)
	a.True(checkEqual(list, -3, -2, 0, -1, 0, 1, 2))
	a.Equal(int32(7), list.Size())
	v, ok = list.RemoveAfterNow()
	a.True(ok)
	a.Equal(0, v)
	a.True(checkEqual(list, -3, -2, 0, -1, 1, 2))
	a.Equal(int32(6), list.Size())
	v, ok = list.RemoveBeforeNow()
	a.True(ok)
	a.Equal(0, v)
	a.True(checkEqual(list, -3, -2, -1, 1, 2))
	a.Equal(int32(5), list.Size())
	list.MovePrev()
	v, ok = list.GetNow()
	a.True(ok)
	a.Equal(-2, v)
	list.Clear()
	a.Equal(int32(0), list.Size())
	v, ok = list.GetNow()
	a.False(ok)
	a.Equal(0, v)
	list.MovePrev()
	a.True(list.InHead())
	v, ok = list.RemoveBeforeNow()
	a.False(ok)
	a.Equal(0, v)
	list.MoveNext().MoveNext()
	a.True(list.InTail())
	list.InsertAfterNow(1)
	a.True(checkEqual(list, 1))
	v, ok = list.RemoveAfterNow()
	a.False(ok)
	a.Equal(0, v)
	cnt := 0
	list.Traversal(func(value int) (needStop bool) {
		cnt++
		v = value
		return true
	})
	a.Equal(1, cnt)
	a.Equal(1, v)
	v, ok = list.RemoveHead()
	a.True(ok)
	a.Equal(1, v)
	v, ok = list.RemoveHead()
	a.False(ok)
	a.Equal(0, v)
	v, ok = list.RemoveTail()
	a.False(ok)
	a.Equal(0, v)
}

func checkEqual(list *LinkList[int], shouldBe ...int) (bool, string) {
	pos := 0
	var (
		equal   = true
		message = ""
	)
	list.Traversal(func(value int) (needStop bool) {
		if value != shouldBe[pos] {
			equal = false
			message = fmt.Sprintf("at pos %d, need %d, got %d", pos, shouldBe[pos], value)
			return true
		}
		pos++
		return false
	})
	return equal, message
}
