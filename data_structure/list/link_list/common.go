package link_list

type linkNode[V any] struct {
	value *V
	pre   *linkNode[V]
	nxt   *linkNode[V]
}
