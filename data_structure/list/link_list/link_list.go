package link_list

import (
	"fmt"
	"strings"
)

type LinkList[V any] struct {
	head *linkNode[V]
	tail *linkNode[V]
	cnt  int32
	now  *linkNode[V]
}

func NewLinkList[V any]() *LinkList[V] {
	head := &linkNode[V]{}
	tail := &linkNode[V]{}
	head.nxt = tail
	tail.pre = head
	return &LinkList[V]{
		head: head,
		tail: tail,
		cnt:  0,
		now:  head,
	}
}

func (list *LinkList[V]) PushHead(value V) *LinkList[V] {
	newNode := &linkNode[V]{
		value: &value,
		pre:   list.head,
		nxt:   list.head.nxt,
	}
	list.head.nxt.pre = newNode
	list.head.nxt = newNode
	list.cnt++
	return list
}

func (list *LinkList[V]) PushTail(value V) *LinkList[V] {
	newNode := &linkNode[V]{
		value: &value,
		pre:   list.tail.pre,
		nxt:   list.tail,
	}
	list.tail.pre.nxt = newNode
	list.tail.pre = newNode
	list.cnt++
	return list
}

func (list *LinkList[V]) InsertBeforeNow(value V) *LinkList[V] {
	now := list.now
	if list.InHead() {
		now = now.nxt
	}
	newNode := &linkNode[V]{
		value: &value,
		pre:   now.pre,
		nxt:   now,
	}
	now.pre.nxt = newNode
	now.pre = newNode
	list.cnt++
	return list
}

func (list *LinkList[V]) InsertAfterNow(value V) *LinkList[V] {
	now := list.now
	if list.InTail() {
		now = now.pre
	}
	newNode := &linkNode[V]{
		value: &value,
		pre:   now,
		nxt:   now.nxt,
	}
	now.nxt.pre = newNode
	now.nxt = newNode
	list.cnt++
	return list
}

func (list *LinkList[V]) MovePrev() *LinkList[V] {
	if list.InHead() {
		return list
	}
	list.now = list.now.pre
	return list
}

func (list *LinkList[V]) MoveNext() *LinkList[V] {
	if list.InTail() {
		return list
	}
	list.now = list.now.nxt
	return list
}

func (list *LinkList[V]) RemoveHead() (V, bool) {
	if list.cnt == 0 {
		return *new(V), false
	}
	retV := list.head.nxt.value
	list.head.nxt = list.head.nxt.nxt
	list.head.nxt.pre = list.head
	list.cnt--
	return *retV, true
}

func (list *LinkList[V]) RemoveTail() (V, bool) {
	if list.cnt == 0 {
		return *new(V), false
	}
	retV := list.tail.pre.value
	list.tail.pre = list.tail.pre.pre
	list.tail.pre.nxt = list.tail
	list.cnt--
	return *retV, true
}

func (list *LinkList[V]) RemoveBeforeNow() (V, bool) {
	if list.InHead() || list.now.pre == list.head || list.cnt == 0 {
		return *new(V), false
	}
	retV := list.now.pre.value
	list.now.pre = list.now.pre.pre
	list.now.pre.nxt = list.now
	list.cnt--
	return *retV, true
}

func (list *LinkList[V]) RemoveAfterNow() (V, bool) {
	if list.InTail() || list.now.nxt == list.tail || list.cnt == 0 {
		return *new(V), false
	}
	retV := list.now.nxt.value
	list.now.nxt = list.now.nxt.nxt
	list.now.nxt.pre = list.now
	list.cnt--
	return *retV, true
}

func (list *LinkList[V]) Clear() {
	list.head.nxt = list.tail
	list.tail.pre = list.head
	list.cnt = 0
	list.now = list.head
}

func (list LinkList[V]) GetNow() (V, bool) {
	if list.InHead() || list.InTail() {
		return *new(V), false
	}
	return *list.now.value, true
}

func (list LinkList[V]) InHead() bool {
	return list.now == list.head
}

func (list LinkList[V]) InTail() bool {
	return list.now == list.tail
}

func (list LinkList[V]) Size() int32 {
	return list.cnt
}

func (list LinkList[V]) Traversal(handler func(value V) (needStop bool)) {
	now := list.head
	for now.nxt != nil && now.nxt != list.tail {
		now = now.nxt
		if handler(*now.value) {
			return
		}
	}
}

func (list LinkList[V]) String() string {
	builder := strings.Builder{}
	builder.WriteString("[head]->")
	list.Traversal(func(value V) (needStop bool) {
		builder.WriteString(fmt.Sprintf("(%+v)->", value))
		return false
	})
	builder.WriteString("[tail]")
	return builder.String()
}
