package link_list

type CircularLinkList[V any] struct {
	cnt int32
	now *linkNode[V]
}

func NewCircularLinkList[V any]() *CircularLinkList[V] {
	return &CircularLinkList[V]{
		cnt: 0,
		now: nil,
	}
}

func (list *CircularLinkList[V]) PushBefore(value V) *CircularLinkList[V] {
	newNode := &linkNode[V]{
		value: &value,
		pre:   nil,
		nxt:   nil,
	}
	if list.cnt == 0 {
		list.pushWhenEmpty(newNode)
	} else {
		newNode.nxt = list.now
		newNode.pre = list.now.pre
		list.now.pre = newNode
		newNode.pre.nxt = newNode
	}
	list.cnt++
	return list
}

func (list *CircularLinkList[V]) PushAfter(value V) *CircularLinkList[V] {
	newNode := &linkNode[V]{
		value: &value,
		pre:   nil,
		nxt:   nil,
	}
	if list.cnt == 0 {
		list.pushWhenEmpty(newNode)
	} else {
		newNode.pre = list.now
		newNode.nxt = list.now.nxt
		list.now.nxt = newNode
		newNode.nxt.pre = newNode
	}
	list.cnt++
	return list
}

func (list *CircularLinkList[V]) RemoveBefore() (V, bool) {
	if list.cnt == 0 {
		return *new(V), false
	}
	if list.cnt == 1 {
		v := list.now.value
		list.now = nil
		list.cnt = 0
		return *v, true
	}
	retV := list.now.pre.value
	list.now.pre = list.now.pre.pre
	list.now.pre.nxt = list.now
	list.cnt--
	return *retV, true
}

func (list *CircularLinkList[V]) RemoveAfter() (V, bool) {
	if list.cnt == 0 {
		return *new(V), false
	}
	if list.cnt == 1 {
		v := list.now.value
		list.now = nil
		list.cnt = 0
		return *v, true
	}
	retV := list.now.nxt.value
	list.now.nxt = list.now.nxt.nxt
	list.now.nxt.pre = list.now
	list.cnt--
	return *retV, true
}

func (list *CircularLinkList[V]) Clear() *CircularLinkList[V] {
	list.cnt = 0
	list.now = nil
	return list
}

func (list *CircularLinkList[V]) MoveNext() *CircularLinkList[V] {
	if list.now != nil {
		list.now = list.now.nxt
	}
	return list
}

func (list *CircularLinkList[V]) MovePrev() *CircularLinkList[V] {
	if list.now != nil {
		list.now = list.now.pre
	}
	return list
}

func (list CircularLinkList[V]) Get() (V, bool) {
	if list.now == nil {
		return *new(V), false
	}
	return *list.now.value, true
}

func (list CircularLinkList[V]) Traversal(handler func(value V) (needStop bool), onlyOneLoop bool) {
	if list.now == nil {
		return
	}
	now, begin := list.now, list.now
	for {
		if handler(*now.value) {
			return
		}
		now = now.nxt
		if onlyOneLoop && now == begin {
			return
		}
	}
}

func (list CircularLinkList[V]) Size() int32 {
	return list.cnt
}

func (list CircularLinkList[V]) Empty() bool {
	return list.cnt == 0
}

// helper function
func (list *CircularLinkList[V]) pushWhenEmpty(node *linkNode[V]) {
	node.pre = node
	node.nxt = node
	list.now = node
}
