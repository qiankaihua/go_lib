package stack

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestStack(t *testing.T) {
	a := require.New(t)
	s := NewStack[int]()
	a.NotNil(s)

	s.Push(123)
	s.Push(111)
	a.Equal(2, s.Size())
	a.False(s.Empty())
	v, ok := s.Top()
	a.True(ok)
	a.Equal(111, v)
	v, ok = s.Pop()
	a.True(ok)
	a.Equal(111, v)
	v, ok = s.Pop()
	a.True(ok)
	a.Equal(123, v)
	v, ok = s.Pop()
	a.False(ok)
	a.Zero(v)
	v, ok = s.Top()
	a.False(ok)
	a.Zero(v)
	a.True(s.Empty())
	for i := 1; i < 10; i++ {
		s.Push(i)
	}
	a.Equal(9, s.Size())
	for i := 9; i >= 1; i-- {
		v, ok = s.Pop()
		a.True(ok)
		a.Equal(i, v)
	}
	v, ok = s.Pop()
	a.False(ok)
	a.Zero(v)
	for i := 1; i < 10; i++ {
		s.Push(i)
	}
	a.Equal(9, s.Size())
	s.Clear()
	a.Zero(s.Size())
	v, ok = s.Pop()
	a.False(ok)
	a.Zero(v)
}
