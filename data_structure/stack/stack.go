package stack

type Stack[V any] struct {
	stack   []*V
	pointer int
	cap     int
}

func NewStack[V any]() *Stack[V] {
	return &Stack[V]{
		stack:   make([]*V, 0, 16),
		pointer: 0,
		cap:     0,
	}
}

func (s *Stack[V]) Push(value V) {
	if s.pointer < s.cap {
		s.stack[s.pointer] = &value
		s.pointer++
		return
	}
	s.stack = append(s.stack, &value)
	s.cap++
	s.pointer++
}

func (s *Stack[V]) Pop() (V, bool) {
	if s.pointer == 0 {
		return *new(V), false
	}
	s.pointer--
	return *s.stack[s.pointer], true
}

func (s *Stack[V]) Top() (V, bool) {
	if s.pointer == 0 {
		return *new(V), false
	}
	return *s.stack[s.pointer-1], true
}

func (s *Stack[V]) Empty() bool {
	return s.pointer == 0
}

func (s *Stack[V]) Clear() {
	s.pointer = 0
}

func (s *Stack[V]) Size() int {
	return s.pointer
}
