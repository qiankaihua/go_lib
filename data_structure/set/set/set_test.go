package set

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestSet(t *testing.T) {
	a := require.New(t)
	s := Set[int]{}
	a.NotNil(s)
	s.Insert(1, 2, 3, 4, 5)
	a.Equal(5, s.Size())
	s.Insert(2, 3, 4, 5, 6)
	a.Equal(6, s.Size())
	s.Remove(1, 2, 3, -1, -2, -3)
	a.Equal(3, s.Size())
	a.True(s.Contains(4))
	a.False(s.Contains(3))
	a.True(s.ContainsAll(4, 5, 6))
	a.False(s.ContainsAll(4, 5, 6, 1))
}
