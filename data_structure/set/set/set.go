package set

type Set[V comparable] map[V]struct{}

func (s *Set[V]) Insert(v ...V) {
	for _, vv := range v {
		(*s)[vv] = struct{}{}
	}
}

func (s *Set[V]) Remove(v ...V) {
	for _, vv := range v {
		delete(*s, vv)
	}
}

func (s *Set[V]) Size() int {
	return len(*s)
}

func (s *Set[V]) Contains(v V) bool {
	_, ok := (*s)[v]
	return ok
}

func (s *Set[V]) ContainsAll(v ...V) bool {
	for _, vv := range v {
		if _, ok := (*s)[vv]; !ok {
			return false
		}
	}
	return true
}
